from flask import Flask, make_response, request, jsonify, Response
from flask_cors import CORS, cross_origin
from dotenv import load_dotenv
from os import path
import os
import json
import pandas as pd
import gspread # https://github.com/burnash/gspread
from oauth2client.service_account import ServiceAccountCredentials
import requests
   
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

scope = ['https://spreadsheets.google.com/feeds']
# credentials = ServiceAccountCredentials.from_json_keyfile_name(f"{basedir}\quiet-platform-333018-b168ef681fa9.json", scope) # rodando localmente
credentials = ServiceAccountCredentials.from_json_keyfile_name('quiet-platform-333018-b168ef681fa9.json', scope) # rodando pelo heroku
client = gspread.authorize(credentials)

# id da planilha do google sheets
docid = "1Sj9Jis6Gq07bCWHd3wC_rN1FuDHrih65FySG7IpNvxQ"

# rota inicial
@app.route('/', methods=['GET'])
def home():
    # teste para verificar se está ok
    return 'funfa'

# dashboards da area
@app.route('/api/tracking/<string:cep>', methods=['GET'])
def getdashcep(cep):
    # abre a planilha do google pelo id
    base = client.open_by_key(docid)
    base_ceps = base.worksheet("base").get_all_values()
    headers = base_ceps[0]
    df_ceps = pd.DataFrame(base_ceps[1:])
    df_ceps.columns = headers

    headers = {'Content-type': 'application/json'}
    resCodCid = requests.get(f'https://viacep.com.br/ws/{cep}/json', headers=headers)
    codCidade = resCodCid.json()

    codCidade = codCidade['ibge']

    resDados = requests.get(f'https://servicodados.ibge.gov.br/api/v1/localidades/municipios/{codCidade}')
    resDados = resDados.json()

    if not (resDados['nome'] in df_ceps['cidade'].values):
        if not (resDados['microrregiao']['nome'] in df_ceps['microrregiao'].values):
            if not (resDados['mesorregiao']['nome'] in df_ceps['mesorregiao'].values):
                if not (resDados['regiao-imediata']['regiao-intermediaria']['UF']['nome'] in df_ceps['uf'].values):
                    res = 'Não encontrado!'
                else:
                    df_dash_ceps = df_ceps[df_ceps['uf'].str.contains(resDados['regiao-imediata']['regiao-intermediaria']['UF']['nome'])]
            else:
                df_dash_ceps = df_ceps[df_ceps['mesorregiao'].str.contains(resDados['mesorregiao']['nome'])]
        else:
            df_dash_ceps = df_ceps[df_ceps['microrregiao'].str.contains(resDados['microrregiao']['nome'])]
    else:
        df_dash_ceps = df_ceps[df_ceps['cidade'].str.contains(resDados['nome'])]

    res = df_dash_ceps.to_dict('records')

    return jsonify(res)


@app.errorhandler(404)
def not_found(error):
    return make_response({'error': 'Not found'}, 404)

@app.errorhandler(500)
def not_found(error):
    return make_response({'error': 'Internal error'}, 500)
   
if __name__ == "__main__":
    app.FLASK_ENV = 'production'
    app.DEBUG = False
    app.TESTING = False
    app.SECRET_KEY = 'LBMtfDCFYjD'
    app.run(host='0.0.0.0', port=8002, debug=False)